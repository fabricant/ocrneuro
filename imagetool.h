#ifndef IMAGETOOL_H
#define IMAGETOOL_H
#include <iostream>

class ImageTool
{
    ImageTool();

public:
    static ImageTool& sharedManager();
    void grayscaleFromRGB(unsigned char* inputImage, unsigned int pixelCount, unsigned int bytePerPixel, unsigned char *outputImage);
    void gausBlurRGB(unsigned char *inputImage, int width, int height, unsigned int bytePerPixel, unsigned char *outputImage);
    void doubleTrashHold(unsigned char* inputImage, unsigned int width, unsigned int height, unsigned int bytePerPixel,
                         float min, float max, unsigned char *outputImage);
    void sobelOperator(unsigned char *inputImage, int width, int height, unsigned int bytePerPixel, bool alpha, unsigned char *outputImage);
    void sobelNonMaximumSuppression(unsigned char *inputImage, int width, int height, unsigned int bytePerPixel, bool alpha,
                                  unsigned char *outputImage);
    void matrix(unsigned char *inputImage, int width, int height, int bytePerPixel,
                float *matrix, int martixWidth, int matrixHeight, float normalizeValue, unsigned char *outputImage);

protected:
    float normalizeGausValue;
    float normalizeSobelValueX;
    float normalizeSobelValueY;
    inline void calculateSum(unsigned char *pixel, float *sum, int &bytePerPixel, float &matrixVlue);
    inline void normilizeSumValues(float *sum, int &valuesCount, unsigned char* outputValues, float matrixSumValue);
    void nonMaximumSuppression(unsigned char *inputImage, unsigned char *Gx, unsigned char *Gy, size_t width, size_t height);
    inline int sign(float x);
};

#endif // IMAGETOOL_H
