#ifndef QUEUMANAGER_H
#define QUEUMANAGER_H

#include <list>
#include <functional>
#include "operationsqueue.h"

class QueueManager
{
protected:
    size_t maxActiveOperationsCount;
    void parse(char*);
    std::list<OperationsQueue>operations;
    std::list<OperationsQueue>::iterator iterator;
public:
    QueueManager();
    void setMaxActiveOperationsCount(size_t x);
    void addOperation(std::function<void()> newOperation);
};

#endif // QUEUMANAGER_H
