#include "queuemanager.h"
#include <thread>
#include <string>

using namespace std;

QueueManager::QueueManager()
{
    setMaxActiveOperationsCount(1);
}

void QueueManager::setMaxActiveOperationsCount(size_t x)
{
    maxActiveOperationsCount = x;
    if (operations.size() < maxActiveOperationsCount)
    {
        size_t needThreadsCount = maxActiveOperationsCount - operations.size();

        for (size_t i = 0; i < needThreadsCount; ++i)
        {
            OperationsQueue operationThread;
            operations.push_back(operationThread);
        }
        iterator = operations.begin();


    }
    else if (operations.size() > maxActiveOperationsCount)  // really slow, need to find better solution
    {
        size_t nedRemoveThreadsCount = operations.size() - maxActiveOperationsCount;

        for (size_t i = 0; i < nedRemoveThreadsCount; ++i)
        {
            std::list<OperationsQueue>::reverse_iterator reverseIterator = operations.rbegin();
            list<function<void ()>> moveOperations = reverseIterator->cutOperations();
            operations.pop_back();
            iterator = operations.begin();

            for(function<void ()> opr: moveOperations)
            {
                addOperation(opr);
            }
        }
    }
}

void QueueManager::addOperation(function<void ()> newOperation)
{
    iterator->addOperation(newOperation);
    ++iterator;

    if (iterator == operations.end())
    {
        iterator = operations.begin();
    }
}
