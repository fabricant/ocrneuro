#ifndef OPERATIONSTHREAD_H
#define OPERATIONSTHREAD_H
#include <mutex>
#include <functional>
#include <list>
class OperationsQueue
{

public:
    OperationsQueue();
    OperationsQueue(const OperationsQueue &otherObject);
    ~OperationsQueue();
    void addOperation(std::function<void()> newOperation);
    std::list<std::function<void ()>> cutOperations();
protected:
        void mainCycle();
        std::list<std::function<void()>> operations;
        std::mutex pauseMutex;

        void cycle();

};

#endif // OPERATIONSTHREAD_H
