#include <thread>
#include "operationsqueue.h"

OperationsQueue::OperationsQueue() {}

OperationsQueue::OperationsQueue(const OperationsQueue &otherObject)
{
    this->operations = otherObject.operations;
}

OperationsQueue::~OperationsQueue()
{
    std::lock_guard<std::mutex> lock(pauseMutex);
    operations.clear();
}

void OperationsQueue::cycle()
{
    while(operations.size())
    {
        auto iterator = operations.begin();

        (*iterator)();
        operations.pop_front();
    }
}



void OperationsQueue::addOperation(std::function<void ()> newOperation)
{
    operations.push_back(newOperation);
    if (operations.size() == 1)
    {
        std::thread mainOperationsThread(&OperationsQueue::cycle, this);
        mainOperationsThread.detach();
    }
}


std::list<std::function<void ()>> OperationsQueue::cutOperations()
{
    std::lock_guard<std::mutex> lock(pauseMutex);
    std::list<std::function<void()>> newList = std::move(operations);

    return newList;
}
