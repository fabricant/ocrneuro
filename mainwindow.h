#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "queue/queuemanager.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
protected:
    QImage processImage(QImage &originalImage);
    QImage image;
    QueueManager queueManager;
    void openImage(QString path);
private slots:
    void on_minTrashHoldValue_valueChanged(int value);
    void on_maxTrashHoldValue_valueChanged(int value);
    void openFile();
    void exit();
};

#endif // MAINWINDOW_H
