#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qdebug.h>
#include <QLabel>
#include <QGridLayout>
#include <QImage>
#include <QFileDialog>
#include "functionprofiler.h"
#include "imagetool.h"
#include "consstants.h"

#define BITS_IN_BYTE 8

using namespace  std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    openImage(":/beautiful-green-nature-02.jpg");
    connect(ui->actionOpen, &QAction::triggered, this, &MainWindow::openFile);
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::exit);
}



MainWindow::~MainWindow()
{
    delete ui;
}

QImage MainWindow::processImage(QImage &originalImage)
{
    ImageTool &imageTool = ImageTool::sharedManager();
    size_t pixelCount = originalImage.width() * originalImage.height();
    uchar *imageBits = originalImage.bits();
    int bytesPerPixel = originalImage.depth() / BITS_IN_BYTE;
    uchar *grayImageBytes = new uchar[pixelCount];
    imageTool.grayscaleFromRGB(imageBits, pixelCount, bytesPerPixel, grayImageBytes);

    uchar *gausImageBytes = new uchar[pixelCount];
    imageTool.gausBlurRGB(grayImageBytes, originalImage.width(), originalImage.height(), 1, gausImageBytes);

    uchar *sobolBytes = grayImageBytes;
    imageTool.sobelOperator(gausImageBytes, originalImage.width(), originalImage.height(), 1, false, sobolBytes);

    uchar *doubleTrashHoldBytes = gausImageBytes;
    imageTool.doubleTrashHold(sobolBytes, originalImage.width(), originalImage.height(),
                              1, ui->minTrashHoldValue->value() / 100.0, ui->maxTrashHoldValue->value() / 100.0, doubleTrashHoldBytes);

    QImage outputImage = QImage(doubleTrashHoldBytes, originalImage.width(),
                                originalImage.height(), originalImage.width() , QImage::Format_Grayscale8, [](void *image)
    {
        uchar *uimage = (uchar*)image;
        delete[] uimage;
    });
    delete[] grayImageBytes;
    delete[] gausImageBytes;
    return outputImage;
}

void MainWindow::openImage(QString path)
{
    image = QImage(path);

    ui->original->setPixmap(QPixmap::fromImage(image));

    QImage outputImage = processImage(image);
    ui->modified->setPixmap(QPixmap::fromImage(outputImage));

}


void MainWindow::on_minTrashHoldValue_valueChanged(int value)
{
    queueManager.addOperation([this]
    {
        QImage outputImage = processImage(image);
        ui->modified->setPixmap(QPixmap::fromImage(outputImage));
    });
}

void MainWindow::on_maxTrashHoldValue_valueChanged(int value)
{
    queueManager.addOperation([this]
    {
        QImage outputImage = processImage(image);
        ui->modified->setPixmap(QPixmap::fromImage(outputImage));
    });
}

void MainWindow::openFile()
{
    QString path = QFileDialog::getOpenFileName(this,
                                                    tr("Open Image"), "~//", tr("Image Files (*.png *.jpg *.bmp)"));
    openImage(path);

}

void MainWindow::exit()
{
    QApplication::quit();
}
