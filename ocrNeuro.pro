#-------------------------------------------------
#
# Project created by QtCreator 2015-07-30T13:02:00
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ocrNeuro
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    imagetool.cpp \
    imageprocessor.cpp \
    functionprofiler.cpp \
    queue/operationsqueue.cpp \
    queue/queuemanager.cpp

HEADERS  += mainwindow.h \
    imagetool.h \
    consstants.h \
    imageprocessor.h \
    functionprofiler.h \
    queue/operationsqueue.h \
    queue/queuemanager.h

FORMS    += mainwindow.ui

DISTFILES +=

RESOURCES += \
    resource/resource.qrc
