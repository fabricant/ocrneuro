#ifndef IMAGEPROCESSOR_H
#define IMAGEPROCESSOR_H
#include <QImage>

class ImageProcessor
{
public:
    ImageProcessor();
    void setGausBloor(bool gaus);
    void setMinTrashHold(float min);
    void setMaxTrashHold(float max);
    void setOriginalImage(QImage &&image);
    void setOriginalImage(QImage image);
    QImage &getOriginalImage();
    QImage& getProcessedImage();
protected:
    bool gausBloor = false;
    float minTrashHold;
    float maxTrashHold;
    QImage originalImage;
    QImage outputImage;
};

#endif // IMAGEPROCESSOR_H
