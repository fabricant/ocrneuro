#include <stddef.h>
#include <math.h>
#include <complex>
#include <iostream>
#include <functionprofiler.h>
#include "imagetool.h"

using namespace std;
float  defaultGausMatrix[] = {0.000789, 0.006581, 0.013347, 0.006581, 0.000789,
                              0.006581, 0.054901, 0.111345, 0.054901, 0.006581,
                              0.013347, 0.111345, 0.225821, 0.111345, 0.013347,
                              0.006581, 0.054901, 0.111345, 0.054901, 0.006581,
                              0.000789, 0.006581, 0.013347, 0.006581, 0.000789};

float MGx[] = {-1, 0, 1,
               -2, 0, 2,
               -1, 0, 1};

float MGy[] = {-1, -2, -1,
               0, 0, 0,
              1, 2, 1};


ImageTool::ImageTool()
{
    normalizeGausValue = 0;

    for (int i =  0; i < 25; ++i)
    {
        normalizeGausValue += defaultGausMatrix[i];
    }
    normalizeSobelValueX = 1;
    normalizeSobelValueY = 1;
}

ImageTool &ImageTool::sharedManager()
{
    static ImageTool imageTool;
    return imageTool;
}

void ImageTool::grayscaleFromRGB(unsigned char* inputImage, unsigned int pixelCount, unsigned int bytePerPixel, unsigned char* outputImage)
{
    for (unsigned int j = 0;  j < pixelCount; ++j)
    {
        unsigned int i = j * bytePerPixel;
        outputImage[j] = 0.299 * inputImage[i] + 0.587 * inputImage[i + 1] + 0.114 * inputImage[i + 2];
    }
}

void ImageTool::gausBlurRGB(unsigned char *inputImage, int width,
                            int height, unsigned int bytePerPixel, unsigned char* outputImage)
{
    matrix(inputImage, width, height, bytePerPixel, defaultGausMatrix, 5, 5, normalizeGausValue, outputImage);
}



 void ImageTool::doubleTrashHold(unsigned char *inputImage, unsigned int width, unsigned int height,
                                 unsigned int bytePerPixel, float min, float max, unsigned char* outputImage)
 {
     size_t pixelCount = 0;

     pixelCount = width * height;
     pixelCount *= bytePerPixel;
     for (size_t i = 0; i <  pixelCount; i += bytePerPixel)
     {
         float x = inputImage[i] / 255.0f;
         if(x >= min && x < max)
         {
             outputImage[i] = 255;
         }
         else
         {
             outputImage[i] = 0;
         }
     }
 }

 void ImageTool::sobelOperator(unsigned char *inputImage, int width, int height, unsigned int bytePerPixel, bool alpha,
                               unsigned char *outputImage)
 {
     unsigned int bytesCount = width * height * bytePerPixel;
     unsigned char *Gx = new unsigned char[bytesCount];
     unsigned char *Gy = new unsigned char[bytesCount];
     matrix(inputImage, width, height, bytePerPixel, MGx, 3, 3, normalizeSobelValueX, Gx);
     matrix(inputImage, width, height, bytePerPixel, MGy, 3, 3, normalizeSobelValueY, Gy);
//     if (alpha)
//     {
//         for (size_t i = 0; i < bytesCount; ++i)
//         {
//             outputImage[i] = sqrt(Gx[i] * Gx[i] + Gy[i] * Gy[i]);
//             if ( (i + 1) %4 == 0)
//             {
//                 outputImage[i] = 255;
//             }
//         }
//     }
//     else
//     {
         for (size_t i = 0; i < bytesCount; ++i)
         {
             outputImage[i] = sqrt(Gx[i] * Gx[i] + Gy[i] * Gy[i]);
         }
         nonMaximumSuppression(outputImage, Gx, Gy, width, height);
     delete [] Gx;
     delete [] Gy;
//     }
 }


 void ImageTool::matrix(unsigned char *inputImage,  int width, int height,  int bytePerPixel,
                        float *matrix, int matrixWidth, int matrixHeight, float normalizeValue, unsigned char *outputImage)
 {

     int middleMatrixWidth = lround(matrixWidth / 2.0);
     int middleMatrixHeight = lround(matrixHeight / 2.0);
     float *sum = new float[bytePerPixel];

     for ( int yImage = 0; yImage < height; ++yImage)
     {
         for ( int xImage = 0; xImage <  width; ++xImage)
         {
             memset(sum, 0, sizeof(float) * bytePerPixel);
             for ( short yMatrix = 0 ; yMatrix < matrixHeight; ++yMatrix)
             {
                 int yPosition = abs(yImage + yMatrix - middleMatrixHeight);

                 for ( short xMatrix = 0; xMatrix < matrixWidth; ++xMatrix)
                 {
                     int xPosition = abs(xImage + xMatrix - middleMatrixWidth);
                     int pixelPosition = yPosition * width + xPosition;
                     pixelPosition *= bytePerPixel;

                     int kernelPosition = yMatrix * matrixWidth + xMatrix;
                     calculateSum(inputImage + pixelPosition, sum, bytePerPixel, matrix[kernelPosition]);
                 }
             }
             int pixelPosition = yImage * width + xImage;
             pixelPosition *= bytePerPixel;
             normilizeSumValues(sum, bytePerPixel, outputImage + pixelPosition, normalizeValue);

         }
     }
     delete [] sum;
 }


 inline void ImageTool::calculateSum(unsigned char *pixel, float *sum,  int &bytePerPixel, float &matrixVlue)
 {
    for ( int i = 0; i < bytePerPixel; ++i)
    {
        sum[i] += pixel[i] * matrixVlue;

    }
 }

 inline void ImageTool::normilizeSumValues(float *sum, int &valuesCount,  unsigned char *outputValues, float matrixSumValue)
 {
     for ( int i = 0; i < valuesCount; ++i)
     {
         outputValues[i] = fabs(sum[i] / matrixSumValue);
     }
 }

 void ImageTool::nonMaximumSuppression(unsigned char *ioImage, unsigned char *Gx, unsigned char *Gy, size_t width, size_t height)
 {
     for (size_t y = 0; y < height; ++y)
     {
         for (size_t x = 0; x < width; ++x)
         {
             size_t i = y * width + x;
            float angle = round (atan2(Gx[i], Gy[i]) / M_PI_4) * M_PI_4 - M_PI_2;
            static float defaultAngle = fabs(angle);
            if (angle == defaultAngle)
            {
                int dx = sign(cos(angle));
                int dy = sign(sin(angle));

                if (ioImage[y * width + x] > ioImage[(y + dy)* width + x + dx])
                    ioImage[(y + dy)* width + x + dx] = 0;
                if (ioImage[y * width + x] < ioImage[(y + dy)* width + x + dx])
                    ioImage[y * width + x] = 0;
            }
         }
     }
 }

 int ImageTool::sign(float x)
 {
     if (x < 0)
         return - 1;
     if (x > 0)
         return 1;
     return 0;
 }
