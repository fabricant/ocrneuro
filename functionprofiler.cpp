#include <iostream>
#include "functionprofiler.h"

FunctionProfiler::FunctionProfiler(const char* name):functionName(name)
{
        start = std::clock();
}

FunctionProfiler::~FunctionProfiler()
{
    duration = ( std::clock() - start );

    std::cout<< functionName << ": " << duration << std::endl;
}
