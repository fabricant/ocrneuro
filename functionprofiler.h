#ifndef FUNCTIONPROFILER_H
#define FUNCTIONPROFILER_H


class FunctionProfiler
{
public:
    FunctionProfiler(const char *name);
    ~FunctionProfiler();
protected:
    std::clock_t start;
    double duration;
    const char* functionName;
};

#endif // FUNCTIONPROFILER_H
